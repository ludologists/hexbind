#!/bin/sh

./setup.sh

cat ../monsters.csv | grep -v '#' | while read -r card 
do
	name="$(echo $card | cut -d'|' -f1)"
	output="pdfs/$name.tex"
	strength="$(echo $card | cut -d'|' -f2)"
	speed="$(echo $card | cut -d'|' -f3)"
	wits="$(echo $card | cut -d'|' -f4)"
	rule="$(echo $card | cut -d'|' -f5)"
	terrain="$(echo $card | cut -d'|' -f6)"
	treasure="$(echo $card | cut -d'|' -f7)"
	
	cp monsters.tex "$output"
	sed -i "s/NAME/$name/g" "$output"
	sed -i "s/RULE/$rule/g" "$output"
	sed -i "s/STRENGTH/$strength/g" "$output"
	sed -i "s/SPEED/$speed/g" "$output"
	sed -i "s/SUBTEXT/$subtext/g" "$output"
	sed -i "s/WITS/$wits/g" "$output"
	sed -i "s/TERRAIN/$terrain/g" "$output"
	sed -i "s/TREASURE/$treasure/g" "$output"
	sed -i "s/IMAGE/$name.png/g" "$output"

done
