#!/bin/sh

./setup.sh

cat ../items.csv | grep -v '#' | while read -r card 
do
	name="$(echo $card | cut -d'|' -f1)"
	output="pdfs/$name.tex"
	rule="$(echo $card | cut -d'|' -f2)"
	subtext="$(echo $card | cut -d'|' -f3)"
	size="$(echo $card | cut -d'|' -f4)"
	price="$(echo $card | cut -d'|' -f5)"
	
	cp items.tex "$output"
	sed -i "s/NAME/$name/g" "$output"
	sed -i "s/RULE/$rule/g" "$output"
	sed -i "s/SUBTEXT/$subtext/g" "$output"
	sed -i "s/cardprice{0}/cardprice{$price}/g" "$output"
	sed -i "s/IMAGE/$name.png/g" "$output"

	if [ "$size" = "L" ];then
		sed -i "s/SIZE/Large/" "$output"
	elif [ "$size" = "M" ];then
		sed -i "s/SIZE/Medium/" "$output"
	elif [ "$size" = "S" ];then
		sed -i "s/SIZE/Small/" "$output"
	else
		sed -i "s/SIZE/$size/" "$output"
	fi

done

