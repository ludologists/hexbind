#!/bin/sh

./setup.sh

questno=0

cat ../quests.csv | grep -v '#' | while read -r card 
do
	questno=$(( questno + 1 ))
	output="pdfs/quest-$questno.tex"
	rule="$(echo $card)"
	
	cp exploration.tex "$output"
	sed -i "s/NAME/Quest $questno/g" "$output"
	sed -i "s/RULE/$rule/g" "$output"
	sed -i "s/SUBTEXT/$subtext/g" "$output"
done
