# The Centre Piece

The centre piece has the compass points written on it.
It is always placed first.

It has the tavern in the centre.

## Tavern

Dead Characters respawn at the tavern.

# Terrain

## Mountains = #7a7a7a Grey

## Forest = #369f00 Green

## River = not added yet

## Town = #a60000 Red

Farm land = #fffe58 Yellow
