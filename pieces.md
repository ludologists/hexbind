# Pieces in the Game

- 12 Hero cards
   * 6 coloured meeples
   * 6 standable Hero tokens
- 12 item cards
   * 6 coloured item-cubes
- 12 place cards
   * 6 coloured place-tokens
- 15 monster tokens
- X Exploration Cards
   - including 20 monster cards
- 13 two-sided board pieces (made of 7 hexes)
- 12 Quest cards
- 30 tokens for Fate / Damage Points

# Quest Card Count

This is a running count of how many times a Quest Target is referenced.
They should be roughly equal.

| Quest Target    | Count |
|:--------------- |:----- |
| Red Item        | 4     |
| Blue Item       | 3     |
| Green Item      | 3     |
| Red Character   | 3     |
| Blue Character  | 4     |
| Green Character | 3     |
| Red Location    | 3     |
| Blue Location   | 3     |
| Green Location  | 4     |
